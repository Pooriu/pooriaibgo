module.exports ={
    pwa: {
        name: "drugstore",
        workboxPluginMode: "InjectManifest",
        workboxOptions: {
            swSrc: "src/service-worker.js"
        }
    }
};