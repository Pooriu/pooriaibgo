self.__precacheManifest=[].concat(self.__precacheManifest || []);

workbox.setConfig({
    debug: true
});

workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

let click_open_url;

workbox.routing.registerRoute(
    new RegExp("https://fonts.(?:googleapis|gstatics).com/(.*)"),
    workbox.strategies.cacheFirst({
        cacheName: "googleapis",
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 30
            })
        ],
        method: "GET",
        cacheableResponse: {statuses: [0, 200]}
    })
);

self.addEventListener("push", function(event){
    let push_message = event.data.text();
    click_open_url= "https://vuemeetup.com";
    const options= {
        body: push_message.body,
        icon: "./img/logo.82b9c7a5.png",
        image: "./photo-1495464101292-552d0b52fe41.jpg",
        vibrate: [200, 100, 200, 100, 200, 100, 200],
        tag: "vibration-sample"
    };
    event.waitUntill(
        self.registration.showNotification("my notification", options)
    );
});

self.addEventListener("notificationclick", function(event){
    const clickedNotification = event.notification;
    clickedNotification.close();
    if(click_open_url){
        const promiseChain = clients.openWindow(click_open_url);
        event.waitUntill(promiseChain);
    }
});
    